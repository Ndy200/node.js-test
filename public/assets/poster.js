$(document).ready(function(){

    $('form').on('submit', function(){
      
        var name = $('input[name="name"]').val();
        var category = $('#category').val();
        var brand = $('input[name="brand"]').val();
        var model = $('input[name="model"]').val();
        var price = $('input[name="price"]').val();

        var pass = {category: category, name: name, brand: brand, model: model , price: price};
 
        $.ajax({
          type: 'POST',
          url: '/jsonlist',
          data: pass,
          success: function(data){
            //do something with the data via front-end framework
           
            location.reload()
          }
        });
  
        return false;
  
    });
  
  });