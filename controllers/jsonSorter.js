'use strict';

const fs = require('fs');
const path = require('path');
var url = require('url');
var bodyParser = require('body-parser');
//var urlencodedParser = bodyParser.urlencoded({extended: true});


let filepath = path.join(__dirname, '..', 'public/product.json');


module.exports = function (app) {


    fs.readFile(filepath, "utf8", (err, data) => {
    if (err) {
        console.log(err);
        res.end('valami baj van RF...');
        return;
    }
    var arrayS = JSON.parse(data);
    app.get('/jsonsort', function(req, res) {

        var sortn = req.query.sort;
        let filter = [];
        let cat = req.query.categories;
        var arrayS_in = arrayS;
        let rta = [];
  
        if (cat) {
        cat.forEach(function (item, index, array) {
       
        rta = arrayS_in.filter(it => it.category === item);  
        if(rta.length != 0) {  
        filter.push(...rta); 
        }
        });
        }   


        if (filter.length != 0) {
        arrayS = filter;
        } 

        if (sortn) {
        var sortname_arrayS = sortn.split('-');
        var sortname = sortname_arrayS[1];
        var sortprice = sortname_arrayS[0];
        }

        if (sortname=='asc') {
        arrayS.sort(function (a, b) {
            return a.name.localeCompare(b.name);
        });
        }
        if (sortname=='desc') {
        arrayS.sort(function (a, b) {
            return b.name.localeCompare(a.name);
        });
        }
        if (sortprice=='asc') {
        arrayS.sort(function (a, b) {
        return a.price.localeCompare(b.price);
        });
        }
        if (sortprice=='desc') {
        arrayS.sort(function (a, b) {
        return b.price.localeCompare(a.price);
        });
        }


        res.render('sortjson', {elemek: arrayS}); 
        //Ha tömbbel elvégzi a műveleteket, vissza kell írni az eredeti állapotot
        arrayS = JSON.parse(data);
        });
});

};