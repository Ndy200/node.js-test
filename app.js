const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const path = require('path');
var jsonController = require("./controllers/jsonController");
var jsonSorter = require("./controllers/jsonSorter");

var app = express();

app.set('view engine', 'ejs');

app.use(express.static('./public'));

//fire controller

jsonController(app);
jsonSorter(app);

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/public/index.html'));

});

app.listen(process.env.server_port,process.env.SERVER_HOST || 3000);

console.log(`Your port is ${process.env.SERVER_PORT}`);
console.log(`Your host is ${process.env.SERVER_HOST}`);